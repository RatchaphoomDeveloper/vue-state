import axios from "axios";

const state = {
  todos: [
    { id: 1, title: "Baby shark" },
    { id: 2, title: "MoMo Shabu" }
  ]
};
const getters = {
  allTodos: state => state.todos
};
const actions = {
  async getTodo({ commit }) {
    axios
      .get("https://jsonplaceholder.cypress.io/todos")
      .then(res => {
        console.log(res.data);
        commit("setTodos", res.data);
      })
      .catch(err => {
        console.warn(err.message);
      });
  },
  async addTodo({ commit }, title) {
    const response = await axios.post(
      "https://jsonplaceholder.typicode.com/todos",
      { title, completed: false }
    );
    commit("newTodo", response.data);
  }
};
const mutations = {
  setTodos: (state, todos) => (state.todos = todos),
  newTodo: (state, todo) => state.todos.unshift(todo)
};

export default {
  state,
  getters,
  actions,
  mutations
};
